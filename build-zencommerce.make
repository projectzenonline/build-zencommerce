; Zen Commerce

core = 7.x
api = 2

projects[drupal][type] = core
projects[drupal][version] = 7.27
projects[drupal][patch][1824820] = "https://drupal.org/files/string-offset-cast-1824820-2.patch"
/*projects[drupal][patch][2003826] = "https://drupal.org/files/2003826-16-check_path_index_uri.patch"*/

projects[commerce_kickstart][type] = profile
projects[commerce_kickstart][download][type] = git
projects[commerce_kickstart][download][url] = http://git.drupal.org/project/commerce_kickstart.git
projects[commerce_kickstart][download][branch] = 7.x-2.x

projects[commerce_kickstart][patch][zencommerce] = https://bitbucket.org/projectzenonline/build-zencommerce/raw/master/commerce_kickstart.patch

projects[commerce_stripe][version] = "1.0-rc6"

projects[zencommerce][type] = module
projects[zencommerce][download][type] = git
projects[zencommerce][download][url] = ssh://git@bitbucket.org/projectzenonline/zencommerce.git

projects[views_slideshow][version] = 3.1

projects[addtoany][version] = 4.5

projects[addanother][version] = 2.1

projects[auto_nodetitle][version] = 1.0

projects[contact_forms][version] = 1.8

projects[commerce_feeds][version] = 1.3

projects[faq][version] = 1.0-rc2

projects[facetapi_multiselect][version] = 1.0-beta1

projects[facetapi_collapsible][version] = 1.1

projects[feeds][version] = 2.0-alpha8

projects[google_analytics][version] = 1.4

projects[job_scheduler][version] = 2.0-alpha3

projects[logintoboggan][version] = 1.3

projects[panels][version] = 3.4

projects[features_extra][version] = 1.0-beta1

projects[views_accordion][version] = 1.0

projects[xmlsitemap][version] = 2.0

projects[youtube][version] = 1.2

projects[zc_demo_theme][type] = theme
projects[zc_demo_theme][download][type] = git
projects[zc_demo_theme][download][url] = "ssh://git@bitbucket.org/projectzenonline/zencommerce-theme.git"
projects[zc_demo_theme][download][branch] = master
projects[zc_demo_theme][directory_name] = "zc_demo_theme"


libraries[commerce_stripe][type] = library
libraries[commerce_stripe][download][type] = git
libraries[commerce_stripe][download][url] = "https://github.com/stripe/stripe-php"
libraries[commerce_stripe][directory_name] = "commerce_stripe"

libraries[jquery.cycle][type] = library
libraries[jquery.cycle][download][type] = git
libraries[jquery.cycle][download][url] = "https://github.com/malsup/cycle.git"
libraries[jquery.cycle][directory_name] = "jquery.cycle"

libraries[json2][type] = library
libraries[json2][download][type] = git
libraries[json2][download][url] = "https://github.com/douglascrockford/JSON-js.git"
libraries[json2][directory_name] = "json2"


