core = 7.x
api = 2


includes[build-zencommerce] = "build-zencommerce.make"

projects[zencommerce][download][url] = "file:///var/aegir/shared_platforms/zencommerce/zencommerce"
projects[zencommerce][download][branch] = "develop"

projects[commerce_kickstart][patch][zencommerce] = "file:///var/aegir/shared_platforms/zencommerce/build-zencommerce/commerce_kickstart.patch"

